# web-www.woelkchen.org

Hugo sources for www.woelkchen.org

# git
```sh
cd site-dir
## only once init repo
git init
git add .
git commit -m 'comment'
git remote add origin teahub@teahub.io:user/repo.git
# push to empty remote repo
git push origin master
## or clone remote repo
git clone teahub@teahub.io:user/repo.git
## and every change
git add .
git commit -m 'comment'
git push origin master
```

# themes
Install *ananke* theme.

```sh
git submodule add https://github.com/budparr/gohugo-theme-ananke.git themes/ananke
echo "theme: ananke" >> config.yaml
```

